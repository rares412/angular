﻿using NotesAPI.DtoModels;
using NotesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI
{
    public interface IOwnerProcessor
    {
        Task<List<OwnerView>> GetOwners();
        Task<bool> AddOwner(OwnerView owner);

        Task<bool> DeleteOwnerById(string id);

        Task<bool> UpdateOwnerById(string id, OwnerView owner);
       
    }
}
