﻿using AutoMapper;
using MongoDB.Driver;
using NotesAPI.DtoModels;
using NotesAPI.EntityModels;
using NotesAPI.Mapping;
using NotesAPI.Models;
using NotesAPI.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI
{
    public class NoteProcessor:INoteProcessor
    {
        private readonly INoteRepository _noteRepository;
        private readonly IMapper _mapper;

        public NoteProcessor(IMapper mapper,INoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
            _mapper = mapper;
        }


        public async Task<NoteView> Add(NoteView note)
        {
            note.Id = Guid.NewGuid().ToString();
            await _noteRepository.Add(_mapper.Map<NoteDomain>(note));
            return note;
        }

        public async Task<List<NoteView>> GetAll()
        {
            var result = await _noteRepository.GetAll();
            
            var resultDto = _mapper.Map<List<NoteView>>(result.ToList());
            return resultDto;
        }

        public async Task<NoteView> GetNotesById(string id)
        {
            var noteDto = _mapper.Map<NoteView>((await _noteRepository.GetNotesById(id)));
            return noteDto;
        }

        public async Task<NoteView> GetNotesByOwnerId(Guid id)
        {
            var noteDto = _mapper.Map<NoteView>((await _noteRepository.GetNotesByOwnerId(id)));
            return noteDto;
        }

        public async Task<bool> DeleteNote(string id)
        {
            return (await _noteRepository.DeleteNote(id));
            
        }

        public async Task<bool> DeleteNoteByOwnerId(Guid ownerId)
        {
            return await _noteRepository.DeleteNoteByOwnerId(ownerId);
        }

        public async Task<bool> DeleteAllNotesByOwnerId(Guid ownerId)
        {
            return await _noteRepository.DeleteAllNotesByOwnerId(ownerId);
        }

        public async Task<NoteView> UpdateNote(string id, NoteView note)
        {
            var noteDomain = _mapper.Map<NoteDomain>(note);
            noteDomain.Id = id;
            return _mapper.Map<NoteView>(await _noteRepository.UpdateNote(id, noteDomain));
        }

        public async Task<NoteView> UpdateNoteByOwnerId(Guid id, NoteView note)
        {
            var noteDomain = _mapper.Map<NoteDomain>(note);
            noteDomain.OwnerId = id;
            return _mapper.Map<NoteView>(await _noteRepository.UpdateNoteByOwnerId(id, noteDomain));
        }
    }
}
