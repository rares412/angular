﻿using NotesAPI.DtoModels;
using NotesAPI.EntityModels;
using NotesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI
{
    public interface INoteProcessor
    {
        Task<List<NoteView>> GetAll();
        Task<NoteView> GetNotesById(string id);
        Task<NoteView> GetNotesByOwnerId(Guid id);

        Task<NoteView> Add(NoteView note);

        Task<bool> DeleteNote(string id);

        Task<bool> DeleteNoteByOwnerId(Guid ownerId);

        Task<bool> DeleteAllNotesByOwnerId(Guid ownerId);

        Task<NoteView> UpdateNote(string id, NoteView note);

        Task<NoteView> UpdateNoteByOwnerId(Guid ownerId, NoteView note);
    }
}
