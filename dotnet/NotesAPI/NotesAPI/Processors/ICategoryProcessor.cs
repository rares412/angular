﻿using NotesAPI.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI
{
    public interface ICategoryProcessor
    {
         Task<bool> Add(CategoryView category);
        Task<List<CategoryView>> Get();

        Task<CategoryView> GetById(string id);

        Task<bool> RemoveById(string id);

        Task<bool> Update(string id,CategoryView category);
    }
}
