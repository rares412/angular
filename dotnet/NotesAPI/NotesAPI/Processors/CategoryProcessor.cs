﻿using AutoMapper;
using MongoDB.Driver;
using NotesAPI.DtoModels;
using NotesAPI.EntityModels;
using NotesAPI.Repositories;
using NotesAPI.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI
{
    public class CategoryProcessor:ICategoryProcessor
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;

        public CategoryProcessor(ICategoryRepository categoryRepository,IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public async Task<List<CategoryView>> Get()
        {
            var result = await _categoryRepository.Get();
            return _mapper.Map<List<CategoryView>>(result.ToList());
        }

        public async Task<bool> Add(CategoryView category)
        {
            await _categoryRepository.Add(_mapper.Map<CategoryDomain>(category));
            return true;
        }

        public async Task<CategoryView> GetById(string id)
        {
            return _mapper.Map<CategoryView>(await _categoryRepository.GetById(id));
        }

        public async Task<bool> RemoveById(string id)
        {
            return await _categoryRepository.RemoveById(id);
        }

        public async Task<bool> Update(string id,CategoryView category)
        {
            return await _categoryRepository.Update(id, _mapper.Map<CategoryDomain>(category));
        }
    }
}
