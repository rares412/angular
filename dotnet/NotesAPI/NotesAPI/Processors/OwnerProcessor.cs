﻿using AutoMapper;
using MongoDB.Driver;
using NotesAPI.DtoModels;
using NotesAPI.EntityModels;
using NotesAPI.Models;
using NotesAPI.Repositories;
using NotesAPI.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI
{
    public class OwnerProcessor : IOwnerProcessor
    {
        private readonly IOwnerRepository _ownerRepository;
        private readonly IMapper _mapper;

        public OwnerProcessor(IMapper mapper,IOwnerRepository ownerRepository)
        {
            _mapper = mapper;
            _ownerRepository = ownerRepository;
        }
        
        
        public async Task<bool> AddOwner(OwnerView owner)
        {
            await _ownerRepository.AddOwner(_mapper.Map<OwnerDomain>(owner));
            return true;
        }

        public async Task<List<OwnerView>> GetOwners()
        {
            var result = await _ownerRepository.GetOwners();
            return _mapper.Map<List<OwnerView>>(result.ToList());
        }

        public async Task<bool> UpdateOwnerById(string id,OwnerView owner)
        {
            return await _ownerRepository.UpdateOwnerById(id, _mapper.Map<OwnerDomain>(owner));

        }

        public async Task<bool> DeleteOwnerById(string id)
        {
            return await _ownerRepository.DeleteOwnerById(id);
        }
    }
}
