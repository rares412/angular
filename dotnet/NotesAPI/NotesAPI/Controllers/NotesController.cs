﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NotesAPI.DtoModels;
using NotesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        private readonly INoteProcessor _noteProcessor;

        public NotesController(INoteProcessor noteProcessor)
        {
            _noteProcessor = noteProcessor;
        }


        /// <summary>
        /// Get all notes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetNotes()
        {
            List<NoteView> notes = await _noteProcessor.GetAll();
            return Ok(notes);
        }

        /// <summary>
        /// Gets a note with matching id
        /// </summary>
        /// <param name="id">Id that will be used to match the note</param>
        /// <returns>Code 200 and matching note</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetNoteById(string id)
        {
            return Ok(await _noteProcessor.GetNotesById(id));
        }


        /// <summary>
        /// Gets a note with matching ownerId
        /// </summary>
        /// <param name="ownerId">OwnerId that will be matched</param>
        /// <returns>Code 200 and mathing note </returns>
        [HttpGet("/Owner/{ownerId}")]
        public async Task<IActionResult> GetNoteByOwnerId(Guid ownerId)
        {
            return Ok(await _noteProcessor.GetNotesByOwnerId(ownerId));
        }

        /// <summary>
        /// Adds a new note
        /// </summary>
        /// <param name="note">Note that will be added</param>
        /// <returns>Code 200 and added note</returns>
        [HttpPost]
        public async Task<IActionResult> CreateNote([FromBody] NoteView note)
        {
            if (note == null)
            {
                return BadRequest("Note can not be null");
            }
            var noteView=await _noteProcessor.Add(note);
            var resouce = noteView;
            return CreatedAtRoute("NoteId", resouce, noteView);

        }


        /// <summary>
        /// Deletes a note with a specified id
        /// </summary>
        /// <param name="id">Id of the note that will be deleted</param>
        /// <returns>Code 200 if note was deleted or Code 404 if it was not</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteNote(string id)
        {
            var result = await _noteProcessor.DeleteNote(id);
            if (result)
            {
                return Ok();
            }
            return NotFound();
        }


        /// <summary>
        /// Deletes a note with a specified ownerId
        /// </summary>
        /// <param name="ownerId">OwnerId of the note that will be deleted</param>
        /// <returns>Code 200 or code 404</returns>
        [HttpDelete("/ownerId/{ownerId}")]
        public async Task<IActionResult> DeleteNoteByOwnerId(Guid ownerId)
        {
            var result = await _noteProcessor.DeleteNoteByOwnerId(ownerId);
            if (result)
            {
                return Ok("Note was deleted");
            }
            return NotFound();
        }


        /// <summary>
        /// Deletes all notes with specified ownerId
        /// </summary>
        /// <param name="ownerId">OwnerId of the notes that will be deleted</param>
        /// <returns>Code 200, Code 404</returns>
        [HttpDelete("/DeleteAll/{ownerId}")]
        public async Task<IActionResult> DeleteAllByOwnerId(Guid ownerId)
        {
            var result = await _noteProcessor.DeleteAllNotesByOwnerId(ownerId);
            if (result)
            {
                return Ok("Note was deleted");
            }
            return NotFound();
        }


        /// <summary>
        /// Updates an existing note based on id
        /// </summary>
        /// <param name="id">The id of the note that will be updated</param>
        /// <param name="note">The new content that will be added</param>
        /// <returns>Code 400 if note is null, Code 200, Code 404</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateNote(string id,[FromBody] NoteView note)
        {
            if(note==null)
            {
                return BadRequest();
            }
            var result=await _noteProcessor.UpdateNote(id, note);
            if(result!=null)
            {
                return Ok(result);
            }
            NoteView routeValue = result;
            return CreatedAtRoute(routeValue, result);
            
        }

        /// <summary>
        /// Updates an existing note based on OwnerID
        /// </summary>
        /// <param name="ownerId">OwnerID of the note that will be updated</param>
        /// <param name="note">The new content</param>
        /// <returns>Code 400,Code 200,Code 404</returns>
        [HttpPut("/ownerId/{id}")]
        public async Task<IActionResult> UpdateNoteByOwnerId(Guid ownerId, [FromBody] NoteView note)
        {
            if (note == null)
            {
                return BadRequest("Note can not be null");
            }
            var result = await _noteProcessor.UpdateNoteByOwnerId(ownerId, note);
            if (result!=null)
            {
                return Ok(result);
            }
            NoteView routeValue = result;
            return CreatedAtRoute(routeValue, result);

        }
    }
}
