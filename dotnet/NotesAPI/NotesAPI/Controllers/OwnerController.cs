﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NotesAPI.DtoModels;
using NotesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OwnerController : ControllerBase
    {
        private readonly IOwnerProcessor _ownerProcessor;

        public OwnerController(IOwnerProcessor ownerProcessor)
        {
            _ownerProcessor = ownerProcessor;
        }


        /// <summary>
        /// Gets all Owners
        /// </summary>
        /// <returns>Code 200 for Succes and list of owners</returns>
        [HttpGet]
        public async Task<IActionResult> GetOwners()
        {
            List<OwnerView> owners = await _ownerProcessor.GetOwners();
            return Ok(owners);
        }



        /// <summary>
        /// Adds a new Owner
        /// </summary>
        /// <param name="owner">Owner that will be added</param>
        /// <returns>The owner that was added and Code 200</returns>
        [HttpPost]
        public async Task<IActionResult> AddOwner([FromBody] OwnerView owner)
        {
            if(owner==null)
            {
                return BadRequest("owner can not be null");
            }
            await _ownerProcessor.AddOwner(owner);
            var routeValues = new OwnerView { Id = owner.Id, Name = owner.Name };
            return CreatedAtRoute(routeValues, owner);
        }


        /// <summary>
        /// Updates owner with a specific id
        /// </summary>
        /// <param name="id">The id of the owner that will be updated</param>
        /// <param name="owner">The new content</param>
        /// <returns>Code 400,Code 200, Code 404</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOwner(string id, OwnerView owner)
        {
            if(owner==null)
            {
                return BadRequest("Owner can not be null");
            }
            var result = await _ownerProcessor.UpdateOwnerById(id, owner);
            if(result)
            {
                return Ok(owner);
            }
            OwnerView routeValue = new OwnerView { Id=owner.Id,Name=owner.Name};
            return CreatedAtRoute(routeValue, owner);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOwner(string id)
        {
            var result = await _ownerProcessor.DeleteOwnerById(id);
            if(result)
            {
                return Ok("Ownwer was deleted");
            }
            return NotFound();
        }


    }
}
