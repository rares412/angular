﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NotesAPI.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {

        private readonly ICategoryProcessor _categoryProcessor;
        public CategoriesController(ICategoryProcessor categoriesManager)
        {
            _categoryProcessor = categoriesManager;
        }

        /// <summary>
        /// Gets all Categories
        /// </summary>
        /// <returns>Code 200 for Succes and the list of Categories</returns>
        [HttpGet]
        public async Task<IActionResult> GetCategories()
        {
            var result = await _categoryProcessor.Get();
            return Ok(result);
        }

        /// <summary>
        /// Gets the Category that matches the id given or code 404 if it does not exist
        /// </summary>
        /// <param name="id">The id of the requested Category</param>
        /// <returns>Code 200 for Succes and the Category that matches the id given
        /// If it does not exist it returns code 404
        /// </returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategoriesById(string id)
        {
            var category =await  _categoryProcessor.GetById(id);
            if (category != null)
            {
                return Ok(category);
            }
            return NotFound();
        }


        /// <summary>
        /// Adds a new category
        /// </summary>
        /// <param name="category">The category that will be added</param>
        /// <returns>the category that was added</returns>
        [HttpPost]
        public async Task<IActionResult> AddCategory([FromBody] CategoryView category)
        {
            await _categoryProcessor.Add(category);
            var routeValues = new Category { Id = category.Id, Name = category.Name };
            return CreatedAtRoute(routeValues, category);
        }

        /// <summary>
        /// Deletes a category with a given id
        /// </summary>
        /// <param name="id">The id of the category that will be deleted</param>
        /// <returns>Code 200 if the category was deleted or Code 404 if it was not deleted</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(string id)
        {
            var result =await _categoryProcessor.RemoveById(id);
            if(result)
            {
                return Ok("Category was deleted");
            }
            return NotFound();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCategory(string id,[FromBody] CategoryView category)
        {
            var result = await _categoryProcessor.Update(id, category);
            if(result)
            {
                return Ok("Category was updated");
            }
            Category routeValue = new Category { Id = category.Id, Name = category.Name };
            return CreatedAtRoute(routeValue, category);
        }
    }
}
