﻿using AutoMapper;
using NotesAPI.DtoModels;
using NotesAPI.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Mapping
{
    public class MapperProfile:Profile
    {
        public MapperProfile()
        {
            CreateMap<NoteDomain, NoteView>();
            CreateMap<NoteView, NoteDomain>();
            CreateMap<CategoryView, CategoryDomain>();
            CreateMap<CategoryDomain, CategoryView>();
            CreateMap<OwnerDomain, OwnerView>();
            CreateMap<OwnerView, OwnerDomain>();
        }

    }
}
