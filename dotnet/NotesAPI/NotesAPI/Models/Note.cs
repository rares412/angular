﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Models
{
    public class Note
    {

        public Note(Note note)
        {
            this.Id = note.Id;
            this.OwnerId = note.OwnerId;
            this.Title = note.Title;
            this.Description = note.Description;
            this.CategoryId = note.CategoryId;
        }

        public Guid Id { get; set; }
        [Required] public Guid OwnerId { get;set; }
        [Required] public string Title { get; set; }
        public string Description { get; set; }
        public string CategoryId { get; set; }
    }
}
