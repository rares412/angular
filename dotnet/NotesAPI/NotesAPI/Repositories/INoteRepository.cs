﻿using NotesAPI.DtoModels;
using NotesAPI.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI
{
    public interface INoteRepository
    {
        Task<List<NoteDomain>> GetAll();
        Task<NoteDomain> GetNotesById(string id);
        Task<NoteDomain> GetNotesByOwnerId(Guid id);

        Task<NoteDomain> Add(NoteDomain note);

        Task<bool> DeleteNote(string id);

        Task<bool> DeleteNoteByOwnerId(Guid ownerId);

        Task<bool> DeleteAllNotesByOwnerId(Guid ownerId);

        Task<NoteDomain> UpdateNote(string id, NoteDomain note);

        Task<NoteDomain> UpdateNoteByOwnerId(Guid ownerId, NoteDomain note);
    }
}
