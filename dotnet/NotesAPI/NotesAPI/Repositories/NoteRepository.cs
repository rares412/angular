﻿using MongoDB.Driver;
using NotesAPI.EntityModels;
using NotesAPI.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Repositories
{
    public class NoteRepository:INoteRepository
    {
        private readonly IMongoCollection<NoteDomain> _notes;

        public NoteRepository(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _notes = database.GetCollection<NoteDomain>(settings.NoteCollectionName);
        }

        public async Task<NoteDomain> Add(NoteDomain note)
        {
            note.Id = Guid.NewGuid().ToString();
            await _notes.InsertOneAsync(note);
            return note;
        }

        public async Task<List<NoteDomain>> GetAll()
        {
            var result = await _notes.FindAsync(note => true);

            var resultDto = result.ToList();
            return resultDto;
        }

        public async Task<NoteDomain> GetNotesById(string id)
        {

            return (await _notes.FindAsync(note => note.Id == id)).FirstOrDefault();
        }

        public async Task<NoteDomain> GetNotesByOwnerId(Guid id)
        {
           
            return (await _notes.FindAsync(note => note.OwnerId == id)).FirstOrDefault();
        }

        public async Task<bool> DeleteNote(string id)
        {
            var result = await _notes.DeleteOneAsync(note => note.Id == id);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> DeleteNoteByOwnerId(Guid ownerId)
        {
            var result = await _notes.DeleteOneAsync(note => note.OwnerId == ownerId);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> DeleteAllNotesByOwnerId(Guid ownerId)
        {
            var result = await _notes.DeleteManyAsync(note => note.OwnerId == ownerId);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }

        public async Task<NoteDomain> UpdateNote(string id, NoteDomain note)
        {
       
            note.Id = id;
            var result = await _notes.ReplaceOneAsync(note => note.Id == id,note);
            if (!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                await _notes.InsertOneAsync(note);
                return note;
            }
            return note;

        }

        public async Task<NoteDomain> UpdateNoteByOwnerId(Guid id, NoteDomain note)
        {
            note.OwnerId = id;
            var result = await _notes.ReplaceOneAsync(note => note.OwnerId == id, note);
            if (!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                await _notes.InsertOneAsync(note);
                return note;
            }
            return note;
        }
    }
}
