﻿using NotesAPI.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Repositories
{
    public interface IOwnerRepository
    {
        Task<List<OwnerDomain>> GetOwners();
        Task<bool> AddOwner(OwnerDomain owner);

        Task<bool> DeleteOwnerById(string id);

        Task<bool> UpdateOwnerById(string id, OwnerDomain owner);
    }
}
