﻿using MongoDB.Driver;
using NotesAPI.EntityModels;
using NotesAPI.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Repositories
{
    public class OwnerRepository:IOwnerRepository
    {
        private readonly IMongoCollection<OwnerDomain> _owners;

        public OwnerRepository(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _owners = database.GetCollection<OwnerDomain>(settings.OwnerCollectionName);
        }

        public async Task<bool> AddOwner(OwnerDomain owner)
        {
            owner.Id = Guid.NewGuid().ToString();
            await _owners.InsertOneAsync(owner);
            return true;
        }

        public async Task<List<OwnerDomain>> GetOwners()
        {
            var result = await _owners.FindAsync(owner => true);
            return result.ToList();
        }

        public async Task<bool> UpdateOwnerById(string id, OwnerDomain owner)
        {
            owner.Id = id;
            var result = await _owners.ReplaceOneAsync(owner => owner.Id == id, owner);
            if (!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                await _owners.InsertOneAsync(owner);
                return false;
            }
            return true;

        }

        public async Task<bool> DeleteOwnerById(string id)
        {
            var result = await _owners.DeleteOneAsync(owner => owner.Id == id);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }
    }
}
