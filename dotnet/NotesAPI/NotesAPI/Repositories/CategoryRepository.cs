﻿using MongoDB.Driver;
using NotesAPI.EntityModels;
using NotesAPI.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Repositories
{
    public class CategoryRepository:ICategoryRepository
    {
        private readonly IMongoCollection<CategoryDomain> _categories;

        public CategoryRepository(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _categories = database.GetCollection<CategoryDomain>(settings.CategoryCollectionName);
        }

        public async Task<List<CategoryDomain>> Get()
        {
            var result = await _categories.FindAsync(category => true);
            return result.ToList();
        }

        public async Task<bool> Add(CategoryDomain category)
        {
            await _categories.InsertOneAsync(category);
            return true;
        }

        public async Task<CategoryDomain> GetById(string id)
        {
            return (await _categories.FindAsync(category => category.Id == id)).FirstOrDefault();
        }

        public async Task<bool> RemoveById(string id)
        {
            var result = await _categories.DeleteOneAsync(category => category.Id == id);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {

                return false;
            }
            return true;

        }

        public async Task<bool> Update(string id, CategoryDomain category)
        {
            var result = await _categories.ReplaceOneAsync(category => category.Id == id, category);
            if (!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                await _categories.InsertOneAsync(category);
                return false;
            }
            return true;
        }
    }
}
