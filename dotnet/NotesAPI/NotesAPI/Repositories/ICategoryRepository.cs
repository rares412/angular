﻿using NotesAPI.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Repositories
{
    public interface ICategoryRepository
    {
        Task<bool> Add(CategoryDomain category);
        Task<List<CategoryDomain>> Get();

        Task<CategoryDomain> GetById(string id);

        Task<bool> RemoveById(string id);

        Task<bool> Update(string id, CategoryDomain category);
    }
}
