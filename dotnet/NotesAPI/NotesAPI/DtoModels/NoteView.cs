﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.DtoModels
{
    public class NoteView
    {
       

        [BsonId]public string Id { get; set; }
        public Guid OwnerId { get; set; }
       /* [JsonProperty(PropertyName="title")]*/public string Title { get; set; }
        public string Description { get; set; }
        public string CategoryId { get; set; }
    }
}
