import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit {

  title: string="title"
  currentColor: string=""
  inputColor: string=""

  constructor() { }

  ngOnInit(): void {
  }

  changeBackground()
  {
    this.currentColor=this.inputColor;
  }

}
