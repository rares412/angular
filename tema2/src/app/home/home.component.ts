import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  category: string = '';
  searchContent: string = '';

  constructor() {}

  ngOnInit(): void {}
  receiveCategory(categId: string) {
    this.category = categId;
    console.log(this.category);
  }

  receiveSearchContent(search: string) {
    this.searchContent = search;
    console.log(this.searchContent);
  }
}
