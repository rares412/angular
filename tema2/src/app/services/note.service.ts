import { Injectable } from '@angular/core';
import { Note } from '../interfaces/note';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NoteService {
  readonly baseUrl = 'https://localhost:44392';
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  idCount: string = '1';

  constructor(private httpClient: HttpClient) {}

  serviceCall() {
    console.log('Note service was called');
  }

  getNotes(): Observable<Note[]> {
    return this.httpClient.get<Note[]>(
      this.baseUrl + `/Notes`,
      this.httpOptions
    );
  }

  getNote(id: string): Observable<Note> {
    let idUrl = '/Notes/' + id;
    return this.httpClient.get<Note>(this.baseUrl + idUrl, this.httpOptions);
  }

  addNote(note: Note) {
    return this.httpClient.post(
      this.baseUrl + '/Notes',
      note,
      this.httpOptions
    );
  }

  getFilteredNotes(categId: string): Observable<Note[]> {
    return this.httpClient
      .get<Note[]>(this.baseUrl + `/Notes`, this.httpOptions)
      .pipe(map((notes) => notes.filter((note) => note.categoryId == categId)));
  }

  getSearchNotes(searchContent: string): Observable<Note[]> {
    return this.httpClient
      .get<Note[]>(this.baseUrl + '/Notes', this.httpOptions)
      .pipe(
        map((notes) =>
          notes.filter(
            (note) =>
              note.title.includes(searchContent) ||
              note.description.includes(searchContent)
          )
        )
      );
  }

  deleteNote(noteId: string): Observable<Note> {
    let idUrl = '/Notes/' + noteId;
    return this.httpClient.delete<Note>(this.baseUrl + idUrl);
  }

  editNote(note: Note): Observable<Note> {
    let idUrl = '/Notes/' + note.id;
    return this.httpClient.put<Note>(this.baseUrl + idUrl, note);
  }
}
