import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { map, Subject, switchMap, takeUntil } from 'rxjs';
import { Note } from '../interfaces/note';
import { NoteService } from '../services/note.service';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss'],
})
export class NoteComponent implements OnInit, OnChanges, OnDestroy {
  private readonly destroy$: Subject<void> = new Subject();
  notes: Note[] = [];

  @Input() selectedCategoryId: string = '';
  @Input() currentSearch: string = '';

  constructor(private noteService: NoteService) {}

  ngOnInit(): void {
    this.noteService.serviceCall();
    this.noteService
      .getNotes()
      .pipe(takeUntil(this.destroy$))
      .subscribe((note) => {
        this.notes = note;
      });
  }

  ngOnChanges() {
    if (this.selectedCategoryId) {
      this.noteService
        .getFilteredNotes(this.selectedCategoryId)
        .pipe(takeUntil(this.destroy$))
        .subscribe((note) => {
          this.notes = note;
        });
    }

    if (this.currentSearch) {
      this.noteService
        .getSearchNotes(this.currentSearch)
        .pipe(takeUntil(this.destroy$))
        .subscribe((note) => (this.notes = note));
      this.currentSearch = '';
    }
  }

  getId(id: string) {
    this.noteService
      .deleteNote(id)
      .pipe(
        switchMap((x) => this.noteService.getNotes()),
        takeUntil(this.destroy$)
      )
      .subscribe((notes) => (this.notes = notes));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }
}
