import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyRoutingComponent } from './dummy-routing.component';

describe('DummyRoutingComponent', () => {
  let component: DummyRoutingComponent;
  let fixture: ComponentFixture<DummyRoutingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DummyRoutingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyRoutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
