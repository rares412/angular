import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-dummy-routing',
  templateUrl: './dummy-routing.component.html',
  styleUrls: ['./dummy-routing.component.scss'],
})
export class DummyRoutingComponent implements OnInit {
  id: string | null = '';

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.id = this._activatedRoute.snapshot.paramMap.get('id');
  }
}
