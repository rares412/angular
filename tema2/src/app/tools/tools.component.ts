import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss'],
})
export class ToolsComponent implements OnInit {
  title: string = 'Add note';
  titleColor: string = 'red';
  noteContent: string = '';
  addedContent: string = '';
  searchContent: string = '';

  @Output() emitSearch = new EventEmitter<string>();
  constructor() {}

  ngOnInit(): void {}

  setTitle() {
    this.title = 'test';
  }
  addContentToNote() {
    this.addedContent = this.noteContent;
  }

  selectSearch(searchContent: string) {
    this.emitSearch.emit(searchContent);
  }
}
