import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'categoryName',
})
export class CategoryNamePipe implements PipeTransform {
  transform(value: string, id: string): string {
    if (id == '1') return 'To Do';
    if (id == '2') return 'Doing';
    return 'Done';
  }
}
