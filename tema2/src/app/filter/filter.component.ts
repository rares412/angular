import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Category } from '../interfaces/category';
import { FilterService } from '../services/filter.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  categories: Category[] = [];
  @Output() emitSelectedFilter = new EventEmitter<string>();
  constructor(private filterService: FilterService) {}

  ngOnInit(): void {
    this.categories = this.filterService.getFilters();
  }

  selectFilter(category: string) {
    this.emitSelectedFilter.emit(category);
  }
}
