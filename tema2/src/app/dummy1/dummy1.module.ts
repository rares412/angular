import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component1Dummy1Component } from './component1-dummy1/component1-dummy1.component';
import { Component2Dummy1Component } from './component2-dummy1/component2-dummy1.component';
import { Dummy2Module } from './dummy2/dummy2.module';
import { ComponentDummy2Component } from './dummy2/component-dummy2/component-dummy2.component';
import { ComponentDummy3Component } from './dummy2/dummy3/component-dummy3/component-dummy3.component';



@NgModule({
  declarations: [
    Component1Dummy1Component,
    Component2Dummy1Component,
  ],
  imports: [
    CommonModule,
    Dummy2Module
  ],
  exports:[
    Component1Dummy1Component,
    Component2Dummy1Component,
    ComponentDummy2Component,
    ComponentDummy3Component
  ]
})
export class Dummy1Module { }
