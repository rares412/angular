import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Component1Dummy1Component } from './component1-dummy1.component';

describe('Component1Dummy1Component', () => {
  let component: Component1Dummy1Component;
  let fixture: ComponentFixture<Component1Dummy1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Component1Dummy1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Component1Dummy1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
