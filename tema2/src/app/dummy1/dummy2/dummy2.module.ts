import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentDummy2Component } from './component-dummy2/component-dummy2.component';
import { Dummy3Module } from './dummy3/dummy3.module';
import { ComponentDummy3Component } from './dummy3/component-dummy3/component-dummy3.component';



@NgModule({
  declarations: [
    ComponentDummy2Component
  ],
  imports: [
    CommonModule,
    Dummy3Module
  ],
  exports:[
    ComponentDummy2Component,
    ComponentDummy3Component
  ]
})
export class Dummy2Module { }
