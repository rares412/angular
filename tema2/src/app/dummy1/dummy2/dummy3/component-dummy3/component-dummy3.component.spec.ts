import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentDummy3Component } from './component-dummy3.component';

describe('ComponentDummy3Component', () => {
  let component: ComponentDummy3Component;
  let fixture: ComponentFixture<ComponentDummy3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentDummy3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentDummy3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
