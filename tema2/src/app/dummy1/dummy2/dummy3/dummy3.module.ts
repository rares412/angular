import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentDummy3Component } from './component-dummy3/component-dummy3.component';



@NgModule({
  declarations: [
    ComponentDummy3Component
  ],
  imports: [
    CommonModule
  ],
  exports:[
    ComponentDummy3Component
  ]
})
export class Dummy3Module { }
