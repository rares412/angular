import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-component-dummy2',
  templateUrl: './component-dummy2.component.html',
  styleUrls: ['./component-dummy2.component.scss']
})
export class ComponentDummy2Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
