import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentDummy2Component } from './component-dummy2.component';

describe('ComponentDummy2Component', () => {
  let component: ComponentDummy2Component;
  let fixture: ComponentFixture<ComponentDummy2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentDummy2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentDummy2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
