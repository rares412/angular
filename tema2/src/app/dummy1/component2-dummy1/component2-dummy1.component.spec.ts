import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Component2Dummy1Component } from './component2-dummy1.component';

describe('Component2Dummy1Component', () => {
  let component: Component2Dummy1Component;
  let fixture: ComponentFixture<Component2Dummy1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Component2Dummy1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Component2Dummy1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
