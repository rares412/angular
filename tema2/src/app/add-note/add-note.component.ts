import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Route } from '@angular/router';
import { Category } from '../interfaces/category';
import { Note } from '../interfaces/note';
import { NoteComponent } from '../note/note.component';
import { FilterService } from '../services/filter.service';
import { NoteService } from '../services/note.service';
import { skip, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss'],
})
export class AddNoteComponent implements OnInit, OnDestroy {
  private readonly destroy$: Subject<void> = new Subject();

  note: Note = { id: '', title: '', description: '', categoryId: '' };

  noteFormGroup: FormGroup = new FormGroup({});

  noteId: string = '';
  pageTitle: string = 'Add Note';
  buttonText: string = 'Add';

  categories: Category[] = [];

  constructor(
    private _activatedRoute: ActivatedRoute,
    private noteService: NoteService,
    private filterService: FilterService
  ) {}

  ngOnInit(): void {
    this._activatedRoute.params
      .pipe(takeUntil(this.destroy$))
      .subscribe((queryParams: Params) => {
        this.noteId = queryParams['id'];
        console.log(this.noteId);
      });

    this.noteFormGroup = new FormGroup({
      noteTitle: new FormControl(this.note.title, [
        Validators.required,
        Validators.minLength(4),
      ]),
      noteDescription: new FormControl(this.note.description, [
        Validators.required,
      ]),
      noteCategory: new FormControl(this.note.categoryId, [
        Validators.required,
      ]),
    });

    if (this.noteId) {
      this.noteService.getNote(this.noteId).subscribe((note) => {
        this.note = note;
        this.noteFormGroup = new FormGroup({
          noteTitle: new FormControl(this.note.title, [
            Validators.required,
            Validators.minLength(4),
          ]),
          noteDescription: new FormControl(this.note.description, [
            Validators.required,
          ]),
          noteCategory: new FormControl(this.note.categoryId, [
            Validators.required,
          ]),
        });
      });
      this.pageTitle = 'Edit Note';
      this.buttonText = 'Edit';
      console.log(this.note);
    }

    console.log(this.noteFormGroup);

    this.categories = this.filterService.getFilters();
  }

  addNote() {
    this.note.title = this.noteFormGroup.value.noteTitle;
    this.note.description = this.noteFormGroup.value.noteDescription;
    this.note.categoryId = this.noteFormGroup.value.noteCategory;
    this.note.id = this.noteId;
    if (this.noteId) {
      this.noteService.editNote(this.note).subscribe();
      return;
    }
    this.noteService
      .addNote(this.note)
      .pipe(takeUntil(this.destroy$))
      .subscribe();
  }
  get title() {
    return this.noteFormGroup.get('noteTitle');
  }
  get description() {
    return this.noteFormGroup.get('noteDescription');
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }
}
