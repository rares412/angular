import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Note } from '../interfaces/note';
import { NoteService } from '../services/note.service';

import { AddNoteComponent } from './add-note.component';

describe('AddNoteComponent', () => {
  let component: AddNoteComponent;
  let fixture: ComponentFixture<AddNoteComponent>;

  beforeEach(async () => {
    class noteServiceStub {
      addNote() {}
    }

    await TestBed.configureTestingModule({
      declarations: [AddNoteComponent],
      providers: [
        {
          provides: NoteService,
          useFactory: () => noteServiceStub,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {});

  it('should create', () => {
    fixture = TestBed.createComponent(AddNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('should add a note', () => {
    fixture = TestBed.createComponent(AddNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const noteFormGroupStub = new FormGroup({
      noteTitleStub: new FormControl('test', [
        Validators.required,
        Validators.minLength(4),
      ]),
      noteDescriptionStub: new FormControl('test1', [Validators.required]),
      categoryStub: new FormControl('1', [Validators.required]),
    });

    component.noteFormGroup = noteFormGroupStub;
    component.noteId = '';

    const service = TestBed.inject(NoteService);
    const addNoteSpy = spyOn(service, 'addNote');
    const editNoteSpy = spyOn(service, 'editNote');

    component.addNote();
    expect(editNoteSpy).toHaveBeenCalledTimes(0);
    expect(addNoteSpy).toHaveBeenCalledTimes(1);
    expect(component.note.title).toEqual('test');
    expect(component.note.description).toEqual('test1');
    expect(component.note.categoryId).toEqual('1');
    expect(component.note.id).toEqual('');
  });

  it('should edit a note', () => {
    fixture = TestBed.createComponent(AddNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const noteFormGroupStub = new FormGroup({
      noteTitleStub: new FormControl('test', [
        Validators.required,
        Validators.minLength(4),
      ]),
      noteDescriptionStub: new FormControl('test1', [Validators.required]),
      categoryStub: new FormControl('1', [Validators.required]),
    });

    component.noteFormGroup = noteFormGroupStub;
    component.noteId = '1';

    const service = TestBed.inject(NoteService);
    const addNoteSpy = spyOn(service, 'addNote');
    const editNoteSpy = spyOn(service, 'editNote');

    component.addNote();
    expect(addNoteSpy).toHaveBeenCalledTimes(0);
    expect(editNoteSpy).toHaveBeenCalledTimes(1);
    expect(component.note.title).toEqual('test');
    expect(component.note.description).toEqual('test1');
    expect(component.note.categoryId).toEqual('1');
    expect(component.note.id).toEqual('1');
  });
});
