import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appTextColor]'
})
export class TextColorDirective {

  constructor(private el: ElementRef) {

  }


  ngOnInit() {
    this.changeColor(this.appTextColor);
  }

  @Input() appTextColor: string = "";


  private changeColor(color: string) {
    this.el.nativeElement.style.color = color;
  }

}
