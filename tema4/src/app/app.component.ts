import { Component, Directive, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tema4';
  userInputs: string[] = [""];
  inputText = "";
  color: string = "";


  addToList() {
    this.userInputs.push(this.inputText);
  }

}
