import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlightColor]'
})
export class HighlightColorDirective {

  constructor(private el:ElementRef) { }

  @Input() appHighlightColor: string = "";

  @HostListener('mouseenter')
  onMouseEnter() {
    this.highlightColor(this.appHighlightColor);
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.highlightColor("");
  }


  private highlightColor(color: string) {
    this.el.nativeElement.style.color = color;
  }
}
